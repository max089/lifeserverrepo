package com.gurin.conwayslife.server.handler;

import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gurin.conwayslife.server.data.GameField;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

public abstract class HttpResponseFactory {
	private static Logger logger = LoggerFactory.getLogger(HttpResponseFactory.class);
	private HttpResponseFactory(){}
	
	public static HttpResponse createInvalidRequestResponse(ChannelHandlerContext ctx){
		if (logger.isDebugEnabled()){
			logger.debug("Creating response for Invalid Request Method");
		}
		ByteBuf content = ctx.alloc().buffer();
		content.writeBytes("Only POST is allowed here".getBytes());
		DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST,content);
		response.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
        response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
        if (logger.isTraceEnabled()){
        	logger.trace(response.toString());
        }
        return response;
	}

	public static HttpResponse createInvalidContentResponse(ChannelHandlerContext ctx) {
		if (logger.isDebugEnabled()){
			logger.debug("Creating response for Invalid Content type");
		}
		ByteBuf content = ctx.alloc().buffer();
		content.writeBytes("Only application/json is allowed".getBytes());
		DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE,content);
		response.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
        response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
        if (logger.isTraceEnabled()){
        	logger.trace(response.toString());
        }
        return response;
	}

	public static HttpResponse createInvalidJsonResponse(ChannelHandlerContext ctx,String json,Exception ex) {
		if (logger.isDebugEnabled()){
			logger.debug("Creating response for Invalid JSON request");
		}
		ByteBuf content = ctx.alloc().buffer();
		content.writeBytes(("Received invalid JSON: ["+json+"]\n"+ex.getMessage()).getBytes());
		DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE,content);
		response.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
        response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
        if (logger.isTraceEnabled()){
        	logger.trace(response.toString());
        }
        return response;
	}
	
	public static HttpResponse createCalculatedResponse(ChannelHandlerContext ctx,String json){
		if (logger.isDebugEnabled()){
			logger.debug("Creating sucessed response");
		}
		ByteBuf content = ctx.alloc().buffer();
		content.writeBytes(json.getBytes());
		DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,content);
		response.headers().set(CONTENT_TYPE, "application/json");
        response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
        if (logger.isTraceEnabled()){
        	logger.trace(response.toString());
        }
        return response;
	}
}
