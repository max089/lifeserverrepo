package com.gurin.conwayslife.server.handler;

import java.io.ByteArrayOutputStream;
import java.net.ContentHandler;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.gurin.conwayslife.server.FieldCalculator;
import com.gurin.conwayslife.server.data.GameField;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;

public class HttpLifeFieldHandler extends
		SimpleChannelInboundHandler<DefaultFullHttpRequest> {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private FieldCalculator calc = new FieldCalculator();

	@Override
	protected void channelRead0(ChannelHandlerContext ctx,
			DefaultFullHttpRequest request) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Processing request...");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Checking if method is POST");
		}
		HttpMethod method = request.getMethod();
		if (!HttpMethod.POST.equals(method)) {
			logger.warn("Request method is not POST");
			ctx.writeAndFlush(HttpResponseFactory
					.createInvalidRequestResponse(ctx));
			return;
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Checking if content-type is application/json");
		}
		String contentType = request.headers().get(
				HttpHeaders.Names.CONTENT_TYPE);
		if (contentType == null || contentType.isEmpty()
				|| !contentType.startsWith("application/json")) {
			logger.warn("Request content-type is not application/json");
			ctx.writeAndFlush(HttpResponseFactory
					.createInvalidContentResponse(ctx));
			return;
		}

		ByteBuf buffer = request.content();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		buffer.readBytes(baos, buffer.readableBytes());
		String jsonField = baos.toString();
		GameField field = null;
		Gson gson = new Gson();
		if (logger.isDebugEnabled()) {
			logger.debug("Checking if content is a valid fiedl JSON");
		}
		try {
			field = gson.fromJson(jsonField, GameField.class);
			GameField newField = calc.calculate(field);
			String json = gson.toJson(newField);
			if (logger.isTraceEnabled()) {
				logger.trace("Old field state: [{}], new field state: [{}]",
						jsonField, json);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("Next field generation calculated!");
			}
			ctx.writeAndFlush(HttpResponseFactory.createCalculatedResponse(ctx,
					json));
		} catch (JsonSyntaxException jse) {
			logger.warn("Request content is not valid JSON", jse);
			ctx.writeAndFlush(HttpResponseFactory.createInvalidJsonResponse(
					ctx, jsonField, jse));
		}

	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		// TODO Auto-generated method stub
		ChannelFuture f = ctx.close();
		f.await();
	}

}
