package com.gurin.conwayslife.server.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GameField {
	@SerializedName("width")
	private final int width;
	@SerializedName("heigth")
	private final int height;
	@SerializedName("cells")
	Set<Cell> cells = new HashSet<Cell>();

	public GameField(boolean inverted) {
		width = 80;
		height = 80;
	}

	public GameField(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public GameField(int width, int height,Set<Cell> cells) {
		this.cells = cells;
		this.width = width;
		this.height = height;
	}  

	public boolean isCellAlive(Integer x, Integer y) {
		Cell cell = new Cell(x, y);
		return cells.contains(cell);
	}
	
	public boolean isCellDead(Integer x, Integer y){
		return !isCellAlive(x,y);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public void addCell(Cell cell){
		if (cell.getX()>=0 && cell.getX()<=width && cell.getY()>=0 && cell.getY()<=width){
			cells.add(cell);
		}
	}
	
	public void addCell(Integer x,Integer y){
		addCell(new Cell(x,y));
	}
	
	public void removeCell(Cell cell){
		cells.remove(cell);
	}
	
	public void removeCell(Integer x,Integer y){
		cells.remove(new Cell(x,y));
	}

	public Set<Cell> getCells() {
		return cells;
	}

	@Override
	public String toString() {
		return "GameField [width=" + width + ", height=" + height + ", cells="
				+ cells +"]";
	}
	
}
