package com.gurin.conwayslife.server;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.gurin.conwayslife.server.data.Cell;
import com.gurin.conwayslife.server.data.GameField;

public class FieldCalculator {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	public GameField calculate(GameField field) {
		if (logger.isDebugEnabled()){
			logger.debug("Start calculating new field generation...");
		}
		int width = field.getWidth();
		int heigt = field.getHeight();
		int[][] arrayField = new int[width][heigt];
		int[][] newArrayField = new int[width][heigt];
		Set<Cell> cells = field.getCells();
		for (Cell cell : cells) {
			arrayField[cell.getX()][cell.getY()] = 1;
		}
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < heigt; j++) {
				// � ������ (������) ������, ����� � ������� ����� ��� �����
				// ������, ����������� �����;
				if (arrayField[i][j] == 0) {
					int count = countAliveNeighboors(arrayField,i,j);
					if (count == 3) {
						newArrayField[i][j] = 1;
					}
				} else {
					// ���� � ����� ������ ���� ��� ��� ��� ����� �������, ��
					// ��� ������ ���������� ����; � ��������� ������ (����
					// ������� ������ ���� ��� ������ ���) ������ ������� (���
					// ����������� ��� ��� ���������������).
					int count = countAliveNeighboors(arrayField, i, j);
					if (count == 2 || count == 3){
						newArrayField[i][j] = 1;
					} else {
						newArrayField[i][j] = 0;
					}
				}
			}
		}
		// ������ ���� ��������
		GameField newField = new GameField(width,heigt);
		for (int i=0;i<width;i++){
			for (int j=0;j<heigt;j++){
				if (newArrayField[i][j] == 1){
					newField.addCell(i, j);
				}
			}
		}
		if (logger.isDebugEnabled()){
			logger.debug("New field generation calculated");
		}
		return newField;
	}
    // ��������� ����� ������� � ������ ������
	private int countAliveNeighboors(int[][] arrayField,int i, int j) {
		return getCellVal(arrayField, i - 1, j - 1)
				+ getCellVal(arrayField, i, j - 1)
				+ getCellVal(arrayField, i + 1, j - 1)
				+ getCellVal(arrayField, i - 1, j)
				+ getCellVal(arrayField, i + 1, j)
				+ getCellVal(arrayField, i - 1, j + 1)
				+ getCellVal(arrayField, i, j + 1)
				+ getCellVal(arrayField, i + 1, j + 1);
	}
	
	// ������� ����� ������, ����� �� ������� � ���������
	private int getCellVal(int[][] arrayField, int x, int y) {
		if (x < 0 || x >= arrayField.length || y < 0 || y >= arrayField[0].length) {
			return 0;
		} else {
			return arrayField[x][y];
		}
	}
}
