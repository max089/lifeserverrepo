package com.gurin.conwayslife.server;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gurin.conwayslife.server.handler.HttpLifeFieldHandler;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;

public class ServerStartup {
	private static final Logger logger = LoggerFactory.getLogger(ServerStartup.class);
	
	public static void main(String[] args) throws Exception {
		initLog4jProperties();
		int port;
		if (args.length == 0) {
			port = 8080;
			logger.info("Using standart port [{}]",port);
		} else {
			try {
				port = Integer.parseInt(args[0]);
				if (port < 1 || port > Math.pow(2, 32)) {
					throw new Exception("Invalid port " + port);
				}
				logger.info("Using user-defined port [{}]",port);
			} catch (Exception x) {
				throw new IllegalArgumentException("Invalid port number: "
						+ args[0], x);
			}
		}
		ServerStartup startup = new ServerStartup();
		startup.run(port);
	}

	public void run(int port) throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup(); // (1)
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		logger.info("Start initializing server...");
		try {
			ServerBootstrap bootstrap = new ServerBootstrap();
			bootstrap.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.childHandler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel ch)
								throws Exception {
							ChannelPipeline pipeline = ch.pipeline();
							pipeline.addLast("decoder",
									new HttpRequestDecoder());
							pipeline.addLast("aggregator",
									new HttpObjectAggregator(1048576));
							pipeline.addLast("encoder",
									new HttpResponseEncoder());
							pipeline.addLast("handler",
									new HttpLifeFieldHandler());
						}

					}).option(ChannelOption.SO_BACKLOG, 128);
			// .childOption(ChannelOption.SO_KEEPALIVE, true);

			ChannelFuture f = bootstrap.bind(port).sync();
			logger.info("Server started up on port [{}]",port);
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}
	public static void initLog4jProperties(){
		if (System.getProperty("log4j.configuration") == null
				|| System.getProperty("log4j.configuration").isEmpty()) {
			System.setProperty("log4j.configuration",
					"file:/src/main/config/log4j.properties");
		}
		BasicConfigurator.configure();
	}
}
